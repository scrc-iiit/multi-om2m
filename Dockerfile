# syntax=docker/dockerfile:experimental

FROM eclipse-temurin:8-alpine

LABEL maintainer="leojfrancis <leojfrancis.now@gmail.com>"
LABEL description="Latest OM2M IMAGE With Mongo Support"
LABEL version="V1.4.1"


WORKDIR /source
COPY om2m-mongo/ .
COPY conf/om2m-config.ini ./configuration/config.ini

ENTRYPOINT [ "sh", "start.sh" ]
# CMD ["./start.sh"]
# CMD [ "sleep","10000" ]